var operandoUno = 0;
var operandoDos = 0;
var operacionAnterior = "";
var resultado = 0;

function iniciarEventos(){

    let botonesNumeros = document.getElementsByClassName("btn-numero");
    
    for(let i=0; i<botonesNumeros.length; i++){
        botonesNumeros[i].addEventListener("click",generarNumero);
    }

    let botonesOperaciones = document.getElementsByClassName("btn-operacion");
    for(let i=0; i<botonesOperaciones.length; i++){
        botonesOperaciones[i].addEventListener("click", capturarOperacion);
    }
}

function generarNumero(event){
    let numero = event.target.textContent;
    document.getElementById("resultado").value += numero;
}

function capturarOperacion(event){
    operacion = event.target.textContent;
    if(operacion === "="){
        operandoDos = document.getElementById("resultado").value;
        switch(operacionAnterior){
            case "X":
                resultado = parseFloat(operandoUno) * parseFloat(operandoDos);
            break;

            case "/":
                if(operandoDos != 0){
                    resultado = parseFloat(operandoUno) / parseFloat(operandoDos);
                }
                else{
                    resultado = "División por 0";
                }
            break;

            case "+":
                resultado = parseFloat(operandoUno) + parseFloat(operandoDos);
            break;

            case "-":
                resultado = parseFloat(operandoUno) - parseFloat(operandoDos);
            break;
        }
        document.getElementById("resultado").value = resultado;
    }
    else{
        operandoUno = document.getElementById("resultado").value;
        operacionAnterior = event.target.textContent;
        document.getElementById("resultado").value = "";
    }
}