/*
    DOM -> MODELO VIRTUAL DEL HTML DE LA PÁGINA.
        -> ATRIBUTOS
        -> MÉTODOS
        -> EVENTOS.
            ** ELEMENTOS
              -> ATRIBUTOS (VARIABLES).
              -> MÉTODOS (FUNCIONES)
              -> EVENTOS (ACCIONES)
*/





var getById = function(id){
    return document.getElementById(id);
}

function cargarDatos(event){
    datosStorage = localStorage.getItem("datosPersonas");
    if(datosStorage){
        datosStorage = JSON.parse(datosStorage);
        datosStorage.forEach(datosPersona => {
            adicionarFila(datosPersona);            
        });
    }
}

function adicionarFila(datosPersona){
    // innerHTML
    let tabla = document.querySelector("#tabla-datos tbody");
    /*
        FILA -> <tr>
            COLUMNA -> <td> -> valor:nombres.value
            COLUMNA -> <td> -> valor:apellidos.value
            ...
            ...
            COLUMNA -> <td> -> valor:telefono.value
        </tr>
    */
    filas = tabla.childNodes.length + 1;

    let html =  "<tr id='datos-" + filas + "'>";
    html +=     "   <td contenteditable='true'>" + datosPersona.nombres + "</td>";
    html +=     "   <td contenteditable='true'>" + datosPersona.apellidos + "</td>";
    html +=     "   <td contenteditable='true'>" + datosPersona.fechaNacimiento + "</td>";
    html +=     "   <td contenteditable='true'>" + datosPersona.edad + "</td>";
    html +=     "   <td contenteditable='true'>" + datosPersona.estadoCivil + "</td>";
    html +=     "   <td contenteditable='true'>" + datosPersona.telefono + "</td>";
    html +=     "   <td>";
    html +=     "<button type='button' id='editar-" + filas + "' class='editar'><i class='fas fa-edit'></i></button>";
    html +=     "<button type='button' id='eliminar-" + filas + "' class='eliminar'><i class='fas fa-trash'></i></button>";
    html +=     "</tr>";

    tabla.innerHTML += html.trim();

    let botonesEditar = tabla.getElementsByClassName("editar");
    for(let i=0; i<botonesEditar.length; i++){
        botonesEditar[i].addEventListener("click", editar);
    }

    let botoneEliminar = tabla.getElementsByClassName("eliminar");
    for(let i=0; i<botoneEliminar.length; i++){
        botoneEliminar[i].addEventListener("click", eliminar);
    }
}

function guardarDatos(event){
    let nombres = getById("nombres");
    let apellidos = getById("apellidos");
    let fechaNacimiento = getById("fechaNacimiento");
    let edad = getById("edad");
    let telefono = getById("telefono");
    let estadoCivil = document.querySelector("#estadoCivil option:checked");
    let accion = getById("accion");

    if(accion.value === "guardar"){
        let datosPersona = {
            "nombres": nombres.value,
            "apellidos": apellidos.value,
            "fechaNacimiento": fechaNacimiento.value,
            "edad": edad.value,
            "estadoCivil": estadoCivil.textContent,
            "telefono": telefono.value
        }

        adicionarFila(datosPersona);
        /*
            SI ES EL PRIMER ELEMENTO PARA ALMACENAR 
                -> PROBABLEMENTE EL STORAGE NO EXISTA.
                   -> CREAR UN ARRAY CON LOS DATOS.
                ->
                   -> CAPTURAR EL ARRAY CON LOS DATOS
                   -> ADICIONAR LOS NUEVOS DATOS.
        */

        // SI LA CLAVE NO EXISTE DEVUELVE NULO.

        /*
            [
                PERSONA 1 -> OBJECTO CON DATOS PERSONALES,
                PERSONA 2 -> OBJECTO CON DATOS PERSONALES
            ]
        */

        let datosStorage = localStorage.getItem("datosPersonas");

        if(!datosStorage){
            datosStorage = [];        }
        else{
            datosStorage = JSON.parse(datosStorage);
        }

        datosStorage.push(datosPersona);
        localStorage.setItem("datosPersonas",JSON.stringify(datosStorage));

    }
    else{
        let id = getById("idDatos").value;
        let fila = getById(id);
        let datos = fila.getElementsByTagName("td");
        datos[0].textContent = nombres.value;
        datos[1].textContent = apellidos.value;
        datos[2].textContent = fechaNacimiento.value;
        datos[3].textContent = edad.value;
        datos[4].textContent = estadoCivil.textContent;
        datos[5].textContent = telefono.value;
        accion.value = "guardar";
        // 1...
        let index = fila.rowIndex;
        let datosStorage = JSON.parse(localStorage.getItem("datosPersonas"));
        let datosPersona = {
            "nombres": nombres.value,
            "apellidos": apellidos.value,
            "fechaNacimiento": fechaNacimiento.value,
            "edad": edad.value,
            "estadoCivil": estadoCivil.textContent,
            "telefono": telefono.value
        }
        // 0...
        datosStorage[index - 1] = datosPersona;
        localStorage.setItem("datosPersonas", JSON.stringify(datosStorage));
    }

    getById("btnReset").click();
}

function editar(event){
    let fila = event.target.closest("tr");
    let id = fila.id;
    let datos = fila.getElementsByTagName("td");
    let nombres = datos[0].textContent.trim();
    let apellidos = datos[1].textContent.trim();
    let fechaNacimiento = datos[2].textContent.trim();
    let edad = datos[3].textContent.trim();
    let estadoCivil = datos[4].textContent.trim();
    let telefono = datos[5].textContent.trim();

    getById("nombres").value = nombres;
    getById("apellidos").value = apellidos;
    getById("fechaNacimiento").value = fechaNacimiento;
    getById("edad").value = edad;
    getById("estadoCivil").value = estadoCivil === "Soltero" ? "soltero" : estadoCivil === "Casado" ? "casado" : "ulibre";
    getById("telefono").value = telefono;
    getById("accion").value = "editar";
    getById("idDatos").value = id;
}

function eliminar(event){
    // closest();
    let fila = event.target.closest("tr");
    let index = fila.rowIndex;
    datosStorage = JSON.parse(localStorage.getItem("datosPersonas"));
    datosStorage.splice(index - 1, 1);
    localStorage.setItem("datosPersonas",JSON.stringify(datosStorage));
    fila.remove();
}