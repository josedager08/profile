var getById = function(id){
    return document.getElementById(id);
}

function cambioNombre(){
    if(getById("nombres").value.length < 5){
        alert("Digite un nombre de 6 caracteres");
        getById("apellidos").disabled = true;
    }
    else{
        getById("apellidos").disabled = false;
    }
}

function btnGuardar(){
    let nombre = getById("nombres").value;
    let apellidos = document.getElementById("apellidos").value;
    let fechaNacimiento = document.getElementById("fecha-nacimiento").value;
    let direccion = document.getElementById("direccion").value;
    let telefono = document.getElementById("telefono").value;
    let estadoCivil = document.querySelector("input[name='estadoCivil']:checked").value;
    let ciudad = document.querySelector("#ciudades option:checked").textContent;
    let hobbie = document.querySelectorAll("input[type='checkbox']:checked");

    let formClases = getById("fomulario").className;
    
    if(nombre.length < 5){
        let mensajeError = document.createElement("span");
        mensajeError.id = "mensaje-error-nombres";
        mensajeError.textContent = "Por favor digite nombre con 6 o más caracteres";
        //mensajeError.style.color = "red";
        //mensajeError.style.border = "2px red solid";
        mensajeError.classList.add("error", "otraClase");
        
        if(mensajeError.classList.contains("otraClase")){
            mensajeError.classList.remove("otraClase");
        }
        
        document.getElementById("div-nombres").appendChild(mensajeError);
        return false;
    }

    getById("apellidos").disabled = false;

    if(apellidos.length < 5){
        alert("Digite un apellido correcto");
        return;
    }

    getById("fecha-nacimiento").disabled = false;

    let datosPersona = {
        "datosPersonales": {
            "nombres": nombre,
            "apellidos": apellidos,
            "fechaNacimiento": fechaNacimiento
        },
        "datosContacto": {
            "direccion": direccion,
            "telefono": telefono
        }
    }
    
    console.log(datosPersona);
   
    /*
        [
            {"nombre":"Jose","apellido":"Dager"},
            {"nombre":"Isabella", "apellido":"Dager"},
            {"nombre":"Raif","apellido:"Dager""}
        ]
        persona = array[0];
            -> persona.nombre -> Jose
            -> persona.apellido -> Dager

        persona = array[1];
            -> persona.nombre -> Isabella
            -> persona.apellido -> Dager
        
        persona = array[2];
            -> persona.nombre -> Raif
            -> persona.apellido -> Dager

        persona = array[3]; -> undefined. -> Termina el ciclo.
    */
    /*datosPersonales.forEach( persona => {
        console.log("Nombre: " + persona.nombre);
        console.log("Apellidos: " + persona.apellidos);
    });*/

}